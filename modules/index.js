import { commentBotProxy } from "./commentBotProxy";

export const commentBeforeStart = (players) => {
    const commentType = 'beforeStart';
    return commentBotProxy.commentOut(players, commentType);
}

export const commentParticipants = (players) => {
    const commentType = 'participants';
    return commentBotProxy.commentOut(players, commentType);
}

export const commentUpdate = (players) => {
    const commentType = 'update';
    return commentBotProxy.commentOut(players, commentType);
}

export const commentBeforeFinish = (players) => {
    const commentType = 'beforeFinish';
    return commentBotProxy.commentOut(players, commentType);
}

export const commentFinish = (players) => {
    const commentType = 'finish';
    return commentBotProxy.commentOut(players, commentType);
}

export const commentLeaderBoard = (players) => {
    const commentType = 'leaderboard';
    return commentBotProxy.commentOut(players, commentType);
}

