import { commentsFactory } from './commentsFactory';
import { transportList } from './config';
import { getRandomInteger } from './helper';

class CommentBotFacade {
    commentOut (racers, commentType) {
        const sortedRacers = this.sortRacersByProgress(racers);
        const comment = commentsFactory.createComment(commentType, sortedRacers);
        return comment;
    }
  
    giveTransportToPlayers (players) {
        const transportObj = {};
        players.forEach(player => {
            const randomIndex = getRandomInteger(0, transportList.length - 1);
            transportObj[player.username] = transportList[randomIndex];
        })
        return transportObj;
    }
    
    sortRacersByProgress (racers) {
        const sortedPlayers = racers.slice().sort((a, b) => {
            if(a.progressBarWidth === b.progressBarWidth) {
              return a.resultTime > b.resultTime ? 1 : -1;
            }
            return a.progressBarWidth < b.progressBarWidth ? 1 : -1;
          });
        
          return sortedPlayers;
    }
}

export const commentBotFacade = new CommentBotFacade();

