import { 
    comments,
    usernameExp,
    transportExp,
    finishTimeExp
 }  from './config';

class CommentsFactory {
    createComment(type, racers) {
      let comment;
      switch(type) {
        case 'beforeStart': 
            comment = new BeforeStart();
            break;
        case 'participants': 
            comment = new Participants();
            break;
        case 'update':
            comment = new Update();
            break;
        case 'beforeFinish': 
            comment = new BeforeFinish();
            break;
        case 'finish': 
            comment = new Finish();
            break;
        case 'leaderboard': 
            comment = new Leaderboard();
            break;
      }
  
      return getText();

      function getText() {
        const commentWords = comment.text.split(' ');
        
        let usernames = racers.map(racer => racer.username);
        let transports = racers.map(racer => racer.transport);
        let time = racers.map(racer => racer.resultTime);
        
        for(let i = 0; i < commentWords.length; i++) {
            if(commentWords[i] === usernameExp) {
                const username = usernames.shift();
                if(username === undefined) {
                  console.log(commentWords.slice(i));
                  return commentWords.slice(0, i).join(' ');
                }
                commentWords[i] = username;
                continue;
            } else if (commentWords[i] === transportExp) {
                commentWords[i] = transports.shift();
                continue;
            } else if (commentWords[i] === finishTimeExp) {
                commentWords[i] = time.shift();
                continue;
            }
        }

        const result = commentWords.join(' ');
        return result;
      }
    }
  }
  
  class BeforeStart {
    constructor () {
      this.text = comments['beforeStart']; 
    }
  }
  
  class Participants {
    constructor () {
      this.text = comments['participants'];
    }
  }
  
  class Update {
    constructor () {
      this.text = comments['update'];
    }
  }
  
  class BeforeFinish {
    constructor () {
      this.text = comments['beforeFinish'];
    }
  }
  
  class Finish {
    constructor () {
      this.text = comments['finish'];
    }
  }
  
  class Leaderboard {
    constructor () {
      this.text = comments['leaderboard'];
    }
  }
  

export const commentsFactory = new CommentsFactory();
