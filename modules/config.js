export const usernameExp = '@*username*@';
export const transportExp = '@*transport*@';
export const finishTimeExp = '@*finishTime*@';

export const comments = {
    'beforeStart' : 'Hello, everybody! Welcome to the First Universal Interracial KEY_RACES Tournament! Online broadcast will be commented by me - That_Always-Speaking_Person_In_The_Cinema.',
    'participants': 'Anyway, let me introduce you the participants. On the start line there are @*username*@ on rose @*transport*@ , @*username*@ with beatiful @*transport*@ , @*username*@ on powerful @*transport*@ , @*username*@ on @*transport*@ and white @*transport*@ with @*username*@ behind the wheel.',
    'update': '@*username*@ on @*transport*@ leads right now,@*username*@ is one place behind, @*username*@ is on the third position with fantastic @*transport*@. @*username*@ crawls the last.',
    'beforeFinish': 'Several moments left before we define the winner. @*username*@`s @*transport*@ knocks out the rest easily. There is a robust challenge. @*username*@ on his @*transport*@ and @*username*@ on @*transport*@ . Who will brake the game? Let`s see.',
    'finish': 'First who made the final leap is @*username*@  with his ultrasonic @*transport*@. And the last racer arrived is @*username*@ on @*transport*@.',
    'leaderboard': 'Well, well, well... Let me introduce you the leaderboard. The Absolute Champion is @*username*@  with unbelievable time - @*finishTime*@ . On the second place, there is a player @*username*@ . with a result @*finishTime*@ . @*username*@ finished third, nevertheless with time @*finishTime*@ .',
};

export const transportList = [
    'Tesla S', 'Audi TT', 'Suzuki Hayabusa', 'Toyota Sequoia', 'Mercedes Gelandewagen', 
    'KTM Duke', 'Rolls Royce Phantom', 'Yamaha R1', 'Harley Davidson 48'
];
