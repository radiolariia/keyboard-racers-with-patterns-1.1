import { commentBotFacade } from "./commentBotFacade";

class CommentBotProxy {
    constructor() {
        this.api = commentBotFacade;
        this.cache = {};
    }
  
    getTransport (players) {
        if(Object.keys(this.cache).length === 0) {
            this.cache = {
                ...this.api.giveTransportToPlayers(players),
            };
        }
        return this.cache;
    }

    commentOut (players, commentType) {
        console.log(this.cache, 'sg')
        const racers = players.map(player => {
            const transportObj = this.getTransport(players);
            player.transport = transportObj[player.username];
            return player;
        });
        return this.api.commentOut(racers, commentType)
    }
}
export const commentBotProxy = new CommentBotProxy();