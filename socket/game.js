import * as config from './config';
import { texts } from '../public/javascript/data.mjs';
import { MAX_PROGRESS_BAR_WIDTH } from '../public/javascript/config.mjs'
import { 
  commentBeforeStart,
  commentParticipants,
  commentUpdate,
  commentBeforeFinish,
  commentFinish,
  commentLeaderBoard
 } from "../modules/index";

let rooms = {};

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;

    socket.emit("update_rooms", rooms);

    socket.on("create_room", (roomName) => {
      if (rooms[roomName] !== undefined) {
        socket.emit("room_name_error", roomName);
      } else {
        rooms[roomName] = [];
        const players = rooms[roomName];
        const howManyPlayers = players.length;
        

        socket.emit("create_room_done", { roomName, howManyPlayers });
        socket.broadcast.emit("create_room_done", { roomName, howManyPlayers });
      }
    });

    socket.on("join_room", (roomName) => {
        rooms[roomName].push({ 
            username: username, 
            isReady: false, 
            progressBarWidth: 0,
            resultTime: 0,
            gameStartTime: 0, 
        });
        const players = rooms[roomName];
        
        socket.join(roomName, () => {
            io.to(roomName).emit("join_room_done", players);
        });
    });

    socket.on("exit_room", (roomName) => {
      
        const players = rooms[roomName];
        const index = players.findIndex(player => player.username === username);
        players.splice(index, 1);
  
        io.to(roomName).emit("exit_room_done", players);
        socket.emit("update_rooms", rooms);
        socket.leave(roomName);
    });

    socket.on("get_ready", (roomName) => {
        const players = rooms[roomName];
        const player = players.find(player => player.username === username);
        player.isReady = !player.isReady;
  
        io.to(roomName).emit("get_ready_done", players);

        if (everyPlayerIsReady(players)) {
          socket.emit("comment_race", commentBeforeStart(players));
          runTimerBeforeStart(config, roomName, players);
        }
      });

      socket.on("correct_keypress", ({ roomName, progressBarWidth }) => {
        const player = rooms[roomName].find((player) => player.username === username);
        player.progressBarWidth = progressBarWidth;
        player.resultTime = formatSeconds(Date.now() - player.gameStartTime);

        const players = rooms[roomName];
        io.to(roomName).emit("update_progress_bar", players);
      });

      const formatSeconds = (seconds) => {
        return Math.round(seconds / 1000 * 100) / 100;
      }

      const runTimerBeforeStart = (config, roomName, players) => {
        let timeLeft = config.SECONDS_TIMER_BEFORE_START_GAME;
        
        const countdown = setInterval(() => {
          io.to(roomName).emit("run_timer_before_start", timeLeft);

          if(timeLeft < 5) {
            socket.emit("comment_race", commentParticipants(players));
          }

          timeLeft--;
          if (timeLeft < 0) {
            clearInterval(countdown);
            startGame(roomName);
          }
        }, 1000);
      }

      const startGame = (roomName) => {
        let timeLeft = config.SECONDS_FOR_GAME;
        const randomIndex = Math.floor(Math.random() * texts.length);
        const players = rooms[roomName];
        players.map(player => player.gameStartTime = Date.now());
      
        io.to(roomName).emit("start_game", { roomName, randomIndex, timeLeft });
        
        const runGameTimer = setInterval(() => {
          io.to(roomName).emit("run_game_timer", { roomName, timeLeft });

          if(timeLeft === 30) {
            socket.emit("comment_race", commentBeforeFinish(players)); 
          } else if((config.SECONDS_TIMER_BEFORE_START_GAME - timeLeft) % 30 === 0) {
            socket.emit("comment_race", commentUpdate(players));
          }

          timeLeft--;
          if (everyPlayerFinished(players) || timeLeft < 0) {
            socket.emit("comment_race", commentFinish(players));
            clearInterval(runGameTimer);
            const winner = defineWinner(players);

            io.to(roomName).emit("display_winner", { rooms, players, winner });
            socket.emit("comment_race", commentLeaderBoard(players)); 
          }
        }, 1000);
      }
      
      socket.on('disconnect', () => {
        const name = Object.keys(rooms).find((roomName) =>
        rooms[roomName].some((user) => user.username === username));

        if (rooms.hasOwnProperty(name)) {
          rooms[name] = rooms[name].filter(
            (user) => user.username !== username
          );
          if (rooms[name].length === 0) {
            delete rooms[name];
          }
        socket.emit("update_rooms", rooms);
      }});
  });
};

const everyPlayerIsReady = (players) => {
  return players.every((player) => player.isReady === true);
};
const everyPlayerFinished = (players) => {
  return players.every((player) => player.progressBarWidth === MAX_PROGRESS_BAR_WIDTH);
}; 

const defineWinner = (players) => {
  const sortedPlayers = players.slice().sort((a, b) => {
    if(a.progressBarWidth === b.progressBarWidth) {
      return a.resultTime > b.resultTime ? 1 : -1;
    }
    return a.progressBarWidth < b.progressBarWidth ? 1 : -1;
  });

  return sortedPlayers.pop();
}

